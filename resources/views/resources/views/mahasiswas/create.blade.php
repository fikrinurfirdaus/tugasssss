<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Mahasiswa</title>
</head>
<body>
    <h1>Tambah Mahasiswa</h1>

    <form action="{{ route('mahasiswas.store') }}" method="POST">
        @csrf
        <label for="nama">Nama:</label>
        <input type="text" name="nama" required>
        <br>
        <label for="nim">NIM:</label>
        <input type="number" name="nim" required>
        <br>
        <label for="jenis_kelamin">Jenis Kelamin:</label>
        <select name="jenis_kelamin" required>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
        </select>
        <br>
        <label for="prodi">Prodi:</label>
        <input type="text" name="prodi" required>
        <br>
        <label for="fakultas">Fakultas:</label>
        <input type="text" name="fakultas" required>
        <br>
        <button type="submit">Tambah Mahasiswa</button>
    </form>
</body>
</html>
